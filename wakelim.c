//===============================================================================
// File             : wakelim.c
// Date             : 06.10.21
// MCU              : 
// Notes            : "Wake" protocol limited implementation 
//===============================================================================
        

/**** Headers */
    
#include "includes.h"

                    
/**** Local definitions */ 


/**** Global constants */


/**** Local constants */


/**** Global variables */   


/**** Local variables */



/**** Function prototypes */

uint32_t wakelim_Unstuff_It(uint8_t* buf, uint32_t size);
uint32_t wakelim_Stuff_It(uint8_t* buf, uint32_t size, uint32_t size_max);
uint8_t wakelim_CRC8(uint8_t* buf, int32_t size);



//---------------------------------------------------------------
// Function    : wakelim_Unstuff_It
// Description : 
//---------------------------------------------------------------
uint32_t wakelim_Unstuff_It(uint8_t* buf, uint32_t size)
{
uint32_t i;

if(buf[0] != 0xC0)
   {
   return(0);
   }

for(i = 1; i < size - 1; i++)
   {
   if(buf[i] == 0xC0)
      {
      return(0);
      }
   
   if(buf[i] == 0xDB)
      {
      if(buf[i +  1] == 0xDC)
         {
         buf[i] = 0xC0;
         }
      else if(buf[i +  1] == 0xDD)
         {
         } 
      else
         {
         return(0);
         }
         
      memmove((uint8_t*)&buf[i + 1], (uint8_t*)&buf[i + 2], (size - i - 2) * sizeof(uint8_t));
      size--; 
      }
   }

return(size);
}
//------------------------- END OF FUNCTION ---------------------




//---------------------------------------------------------------
// Function    : wakelim_Stuff_It
// Description : 
//---------------------------------------------------------------
uint32_t wakelim_Stuff_It(uint8_t* buf, uint32_t size, uint32_t size_max)
{
int32_t i;

for(i = 1; i < size; i++)
   {
   uint32_t n_to_move = size - (i + 1);
   
   if((buf[i] == 0xC0) || (buf[i] == 0xDB))
      {
      size++;
      
      if(size > size_max)
         {
         return(0);
         }
  
      memmove((uint8_t*)&buf[i + 2], (uint8_t*)&buf[i + 1], n_to_move * sizeof(uint8_t));
         
      if(buf[i] == 0xC0)
         {
         buf[i] = 0xDB;
         buf[i + 1] = 0xDC;
         }
      
      else if(buf[i] == 0xDB)
         {
         buf[i + 1] = 0xDD;
         }
      }
   }  
   
return(size);
}
//------------------------- END OF FUNCTION ---------------------




//---------------------------------------------------------------
// Function    : wakelim_CRC8
// Description : 
//---------------------------------------------------------------
uint8_t wakelim_CRC8(uint8_t* buf, int32_t size)
{
uint32_t i, j;
uint8_t crc8 = 0;

for(i = 0; i < size; i++)
   {
   uint8_t dat = buf[i];
   
   for(j = 0; j < 8;  j++)
      {
      if((dat ^ crc8) & 1)
         {
         crc8 = ((crc8 ^ 0x18) >> 1) | 0x80;
         }
      else
         {
         crc8 = (crc8 >> 1) & ~0x80;
         }
         
      dat >>= 1;
      }
   }   
return(crc8);
}
//------------------------- END OF FUNCTION ---------------------



