//===============================================================================
// File             : wakelim.h
// Date             : 06.10.21
// Notes            :
//===============================================================================


/**** Global definitions */

#define WAKELIM_SELF_ADDRESS 0x2
#define WAKELIM_BROADCAST_ADDRESS 0


/**** External variables */


/**** External functions */

extern uint32_t wakelim_Unstuff_It(uint8_t* buf, uint32_t size);
extern uint32_t wakelim_Stuff_It(uint8_t* buf, uint32_t size, uint32_t size_max);
extern uint8_t wakelim_CRC8(uint8_t* buf, int32_t size);



