//===============================================================================
// File             : i2c_drv.c
// Date             : 05.10.21
// MCU              : 
// Notes            : 
//===============================================================================
        

/**** Headers */
    
#include "includes.h"

                    
/**** Local definitions */ 


/**** Global constants */


/**** Local constants */


/**** Global variables */   

uint8_t i2c_drv_Buffer[I2C_DRV_BUFFER_SIZE];



/**** Local variables */



/**** Function prototypes */

void i2c_drv_Init(void);
void i2c_drv_Write(uint32_t i2c_periph, uint8_t address, uint8_t* buffer, uint32_t length);
void i2c_drv_Read(uint32_t i2c_periph, uint8_t address, uint8_t* buffer, uint32_t length);



//---------------------------------------------------------------
// Function    : i2c_drv_Init
// Description : 
//---------------------------------------------------------------
void i2c_drv_Init(void)
{
// ### I2C0 init

rcu_periph_clock_enable(RCU_I2C0);

gpio_pin_remap_config(GPIO_I2C0_REMAP, ENABLE); // remap I2C0 to PB8, PB9

gpio_init(GPIOB, GPIO_MODE_AF_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_8 | GPIO_PIN_9); // PB8 SCL, PB9 SDA
   
i2c_clock_config(I2C0, 400000, I2C_DTCY_2);
i2c_mode_addr_config(I2C0, I2C_I2CMODE_ENABLE, I2C_ADDFORMAT_7BITS, 0xFF);
i2c_enable(I2C0);
i2c_ack_config(I2C0, I2C_ACK_ENABLE);

// ### I2C1 init

rcu_periph_clock_enable(RCU_I2C1);

gpio_init(GPIOB, GPIO_MODE_AF_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_10 | GPIO_PIN_11); // PB10 SCL, PB11 SDA
   
i2c_clock_config(I2C1, 400000, I2C_DTCY_2);
i2c_mode_addr_config(I2C1, I2C_I2CMODE_ENABLE, I2C_ADDFORMAT_7BITS, 0xFF);
i2c_enable(I2C1);
i2c_ack_config(I2C1, I2C_ACK_ENABLE);
}
//------------------------- END OF FUNCTION ---------------------




//---------------------------------------------------------------
// Function    : i2c_drv_Write
// Description : 
//---------------------------------------------------------------
void i2c_drv_Write(uint32_t i2c_periph, uint8_t address, uint8_t* buffer, uint32_t length)
{
// wait until I2C bus is idle 
while(i2c_flag_get(i2c_periph, I2C_FLAG_I2CBSY));
// send a start condition to I2C bus 
i2c_start_on_bus(i2c_periph);
// wait until SBSEND bit is set 
while(!i2c_flag_get(i2c_periph, I2C_FLAG_SBSEND));
// send slave address to I2C bus 
i2c_master_addressing(i2c_periph, address << 1, I2C_TRANSMITTER);
// wait until ADDSEND bit is set 
while(!i2c_flag_get(i2c_periph, I2C_FLAG_ADDSEND));
// clear ADDSEND bit 
i2c_flag_clear(i2c_periph, I2C_FLAG_ADDSEND);
// wait until the transmit data buffer is empty 
while(!i2c_flag_get(i2c_periph, I2C_FLAG_TBE));

while(length)
   {
   // data transmission 
   i2c_data_transmit(i2c_periph, *buffer);
   // wait until the TBE bit is set 
   while(!i2c_flag_get(i2c_periph, I2C_FLAG_TBE));
   
   buffer++;
   length--;
   }

// send a stop condition to I2C bus 
i2c_stop_on_bus(i2c_periph);
// wait until stop condition generate 
while(I2C_CTL0(i2c_periph)&0x0200);
}
//------------------------- END OF FUNCTION --------------------- 




//---------------------------------------------------------------
// Function    : i2c_drv_Read
// Description : 
//---------------------------------------------------------------
void i2c_drv_Read(uint32_t i2c_periph, uint8_t address, uint8_t* buffer, uint32_t length)
{
// wait until I2C bus is idle 
while(i2c_flag_get(i2c_periph, I2C_FLAG_I2CBSY));
// send a start condition to I2C bus 
i2c_start_on_bus(i2c_periph);
// wait until SBSEND bit is set 
while(!i2c_flag_get(i2c_periph, I2C_FLAG_SBSEND));
// send slave address to I2C bus 
i2c_master_addressing(i2c_periph, address << 1, I2C_RECEIVER);
// wait until ADDSEND bit is set 
while(!i2c_flag_get(i2c_periph, I2C_FLAG_ADDSEND));
// clear ADDSEND bit 
i2c_flag_clear(i2c_periph, I2C_FLAG_ADDSEND);

while(length)
   {
   if(length == 1)
      {
      // disable acknowledge
      i2c_ack_config(i2c_periph, I2C_ACK_DISABLE);
      }
   
   // wait until the RBNE bit is set 
   while(!i2c_flag_get(i2c_periph, I2C_FLAG_RBNE));
   // read data
   *buffer = i2c_data_receive(i2c_periph);

   buffer++;
   length--;
   }

// send a stop condition 
i2c_stop_on_bus(i2c_periph);
// wait until stop condition generate  
while(I2C_CTL0(i2c_periph)&0x0200);
i2c_ackpos_config(i2c_periph, I2C_ACKPOS_CURRENT);
// enable acknowledge 
i2c_ack_config(i2c_periph, I2C_ACK_ENABLE);
}
//------------------------- END OF FUNCTION --------------------- 



