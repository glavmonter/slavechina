
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'parktest' 
 * Target:  'GD32E103CB' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "gd32e10x.h"

#define RTE_DEVICE_STDPERIPHERALS_RCU
#define RTE_DEVICE_STDPERIPHERALS_PMU
#define RTE_DEVICE_STDPERIPHERALS_BKP
#define RTE_DEVICE_STDPERIPHERALS_FMC
#define RTE_DEVICE_STDPERIPHERALS_MISC
#define RTE_DEVICE_STDPERIPHERALS_I2C
#define RTE_DEVICE_STDPERIPHERALS_GPIO
#define RTE_DEVICE_STDPERIPHERALS_TIMER
#define RTE_DEVICE_STDPERIPHERALS_EXTI
#define RTE_DEVICE_STDPERIPHERALS_USART

#endif /* RTE_COMPONENTS_H */
