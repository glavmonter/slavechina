//===============================================================================
// File             : pfm.h
// Date             : 09.09.21
// Notes            :
//===============================================================================

/**** Global definitions */

/////////////////////////////////////////////////////

#define PCB_IN_SYNC_I1           GPIO_PIN_13
#define PCB_IN_SYNC_I1_GPIO_Port GPIOC

#define PCB_IN_INT_DIN           GPIO_PIN_14
#define PCB_IN_INT_DIN_GPIO_Port GPIOC

#define PCB_IN_INT_PORT           GPIO_PIN_15
#define PCB_IN_INT_PORT_GPIO_Port GPIOC

#define PCB_IN_TTERM           GPIO_PIN_0
#define PCB_IN_TTERM_GPIO_Port GPIOA

#define PCB_IN_TTERM_ALERT           GPIO_PIN_1
#define PCB_IN_TTERM_ALERT_GPIO_Port GPIOA

#define PCB_OUT_SYNC_O2           GPIO_PIN_2
#define PCB_OUT_SYNC_O2_GPIO_Port GPIOA

#define PCB_OUT_HEATER           GPIO_PIN_3
#define PCB_OUT_HEATER_GPIO_Port GPIOA

#define PCB_OUT_FAN           GPIO_PIN_4
#define PCB_OUT_FAN_GPIO_Port GPIOA

#define PCB_IN_WGND2_D1           GPIO_PIN_8
#define PCB_IN_WGND2_D1_GPIO_Port GPIOA

#define PCB_IN_WGND2_INT           GPIO_PIN_10
#define PCB_IN_WGND2_INT_GPIO_Port GPIOA

#define PCB_OUT_SYNC_O1           GPIO_PIN_15
#define PCB_OUT_SYNC_O1_GPIO_Port GPIOA

/////////////////////////////////////////////////////

#define PCB_IN_PG_DC5V           GPIO_PIN_1
#define PCB_IN_PG_DC5V_GPIO_Port GPIOB

#define PCB_OUT_EN_DC5V           GPIO_PIN_2
#define PCB_OUT_EN_DC5V_GPIO_Port GPIOB

#define PCB_IN_WGND1_D0           GPIO_PIN_12
#define PCB_IN_WGND1_D0_GPIO_Port GPIOB

#define PCB_IN_WGND1_D1           GPIO_PIN_13
#define PCB_IN_WGND1_D1_GPIO_Port GPIOB

#define PCB_IN_WGND1_INT           GPIO_PIN_14
#define PCB_IN_WGND1_INT_GPIO_Port GPIOB

#define PCB_IN_WGND2_D0           GPIO_PIN_15
#define PCB_IN_WGND2_D0_GPIO_Port GPIOB

#define PCB_OUT_DE            GPIO_PIN_3
#define PCB_OUT_DE_GPIO_Port  GPIOB

#define PCB_OUT_RE            GPIO_PIN_4
#define PCB_OUT_RE_GPIO_Port  GPIOB

#define PCB_IN_SYNC_I2           GPIO_PIN_5
#define PCB_IN_SYNC_I2_GPIO_Port GPIOB

/**** External variables */

/**** External functions */



