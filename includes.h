//===============================================================================
// File             : includes.h
// Date             : 07.10.21
// Notes            :
//===============================================================================


/**** Global includes */

#include "gd32e10x_rcu.h"
#include "cdc_acm_core.h"
#include "usb_delay.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "pb.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "slave.pb.h"

#include "i2c_drv.h"
#include "usb_drv.h"
#include "wakelim.h"
#include "pfm.h"



/**** Global definitions */



