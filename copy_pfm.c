//===============================================================================
// Project          : Parking System Firmware
// Version          : 1.00
// File             : pfm.c
// Date             : 04.10.21
// MCU              : GD32E103CB
// Notes            : For PCBs: park_100
// Author           : RDI Hk
//===============================================================================



/**** Headers */

#include "includes.h"



/**** Local definitions */

#define PFM_VERSION "1.10"

#define PFM_CONFIG_REPLY_PORTS      (1 << 0)
#define PFM_CONFIG_REPLY_RELAYS     (1 << 1)
#define PFM_CONFIG_REPLY_ALL        (1 << 2)


#define PFM_CDC_TX_DELAY 25 // 1=1ms

/**** Type definitions */


typedef struct
{
float Humidity;
float Temperature;
} PFM_TYPE_HUMIDITY_SENSOR;


typedef struct
{
uint8_t ID;
int8_t Temperature;
} PFM_TYPE_TEMPERATURE_SENSOR;



/**** Global constants */


/**** Local constants */
                 

/**** Global variables */

/*
uint8_t test_array[13] = {0xC0, 0x11, 0x22, 0xDB, 0xDC, 0xDB, 0xDC, 0xDB, 0xDD, 0x33, 0x44, 0xDB, 0xDC};
volatile uint32_t stuff_len;
volatile uint32_t unstuff_len;
*/


/**** Local variables */

static uint32_t pfm_m_iConfig = PFM_CONFIG_REPLY_PORTS | PFM_CONFIG_REPLY_RELAYS | PFM_CONFIG_REPLY_ALL;

static volatile uint32_t pfm_Timer;

static volatile uint32_t pfm_Timer_IO;

static volatile uint32_t pfm_Timer_Sensors;

static uint8_t pfm_EIO_Discrete_Inputs_0;
static uint8_t pfm_EIO_Discrete_Inputs_1;

static uint8_t pfm_EIO_Discrete_Outputs_0;
static uint8_t pfm_EIO_Discrete_Outputs_1;

static uint8_t pfm_EIO_Power_Outputs_0;
static uint8_t pfm_EIO_Power_Outputs_1;

static uint8_t pfm_EIO_PortA_0_Inputs;
static uint8_t pfm_EIO_PortA_0_Outputs;

static uint8_t pfm_EIO_PortB_1_Inputs;
static uint8_t pfm_EIO_PortB_1_Outputs;

static PFM_TYPE_HUMIDITY_SENSOR pfm_HumiditySensor;

static PFM_TYPE_TEMPERATURE_SENSOR pfm_TemperatureSensor;

static volatile uint32_t pfm_flag_DC5V_Good;
static volatile uint32_t pfm_flag_Reset_Enabled;


/**** Function prototypes */

void pfm_Systick(void);
void pfm_EXTI(void);

static void pfm_PowerOn(void);
static void pfm_Wake_CDC_Send(uint32_t size);
static uint16_t pfm_DIO_Decode(uint8_t io_1, uint8_t io_0);




//---------------------------------------------------------------
// Function    : main
// Description : 
//---------------------------------------------------------------
int main(void)
{
// ### GPIO clock init
rcu_periph_clock_enable(RCU_GPIOA);
rcu_periph_clock_enable(RCU_GPIOB);
rcu_periph_clock_enable(RCU_GPIOC);
rcu_periph_clock_enable(RCU_GPIOD);
rcu_periph_clock_enable(RCU_AF);


// ### GPIOA init

gpio_pin_remap_config(GPIO_SWJ_SWDPENABLE_REMAP, ENABLE); // disable JTAG except SWD to obtain PB3, PB4, PA15

gpio_bit_reset(PCB_OUT_SYNC_O2_GPIO_Port, PCB_OUT_SYNC_O2);
gpio_bit_reset(PCB_OUT_HEATER_GPIO_Port, PCB_OUT_HEATER_GPIO_Port);
gpio_bit_reset(PCB_OUT_FAN_GPIO_Port, PCB_OUT_FAN);
gpio_bit_reset(PCB_OUT_SYNC_O1_GPIO_Port, PCB_OUT_SYNC_O1);

gpio_init(GPIOA, GPIO_MODE_OUT_PP, GPIO_OSPEED_2MHZ, PCB_OUT_SYNC_O2 | PCB_OUT_HEATER | PCB_OUT_FAN | PCB_OUT_SYNC_O1);

// ### GPIOB init

gpio_bit_reset(PCB_OUT_EN_DC5V_GPIO_Port, PCB_OUT_EN_DC5V);
gpio_bit_reset(PCB_OUT_DE_GPIO_Port, PCB_OUT_DE);
gpio_bit_set(PCB_OUT_RE_GPIO_Port, PCB_OUT_RE);

gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_2MHZ, PCB_OUT_EN_DC5V | PCB_OUT_DE);

gpio_init(GPIOB, GPIO_MODE_OUT_OD, GPIO_OSPEED_50MHZ, PCB_OUT_RE);


// ### GPIOC Init

// ### GPIOD Init


/////////////////////////////////////////////////////////////////////////////////////////////

SystemCoreClockUpdate();

SysTick_Config(SystemCoreClock / 1000);

pfm_PowerOn();

/////////////////////////////////////////////////////////////////////////////////////////////

// ### EXTI init

// enable and set EXTI interrupt to the lowest priority
nvic_irq_enable(EXTI1_IRQn, 2U, 0U);

// connect EXTI line to GPIO pin
gpio_exti_source_select(GPIO_PORT_SOURCE_GPIOB, GPIO_PIN_SOURCE_1); // PG_DC5V signal

// configure EXTI line
exti_init(EXTI_1, EXTI_INTERRUPT, EXTI_TRIG_FALLING);
exti_interrupt_flag_clear(EXTI_1);

/////////////////////////////////////////////////////////////////////////////////////////////

// ### USB init

usb_drv_Init();

/////////////////////////////////////////////////////////////////////////////////////////////

// ### USART0 init

rcu_periph_clock_enable(RCU_USART0);

gpio_pin_remap_config(GPIO_USART0_REMAP, ENABLE); // remap USART0 to PB6, PB7

gpio_init(GPIOB, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_6); // PB6 - UART0_TX
gpio_init(GPIOB, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_7); // PB7 - UART0_RX

usart_deinit(USART0);
usart_baudrate_set(USART0, 115200U);
usart_receive_config(USART0, USART_RECEIVE_ENABLE);
usart_transmit_config(USART0, USART_TRANSMIT_ENABLE);
usart_enable(USART0);

/////////////////////////////////////////////////////////////////////////////////////////////

// ### I2C init

i2c_drv_Init();

/////////////////////////////////////////////////////////////////////////////////////////////


// ##### Setup PCA8535 - Power Outputs
i2c_drv_Buffer[0] = 0x2; // write Output Port 0  
i2c_drv_Buffer[1] = 0;
i2c_drv_Buffer[2] = 0;
i2c_drv_Write(I2C0, 0x22, i2c_drv_Buffer, 3);

i2c_drv_Buffer[0] = 0x6; // write Configuration Port 0  
i2c_drv_Buffer[1] = 0;   // Configuration Port 0
i2c_drv_Buffer[2] = 0;   // Configuration Port 1
i2c_drv_Write(I2C0, 0x22, i2c_drv_Buffer, 3);


// ##### Setup PCA8535 - Discrete Outputs
i2c_drv_Buffer[0] = 0x2; // write Output Port 0  
i2c_drv_Buffer[1] = 0;
i2c_drv_Buffer[2] = 0;
i2c_drv_Write(I2C0, 0x21, i2c_drv_Buffer, 3);

i2c_drv_Buffer[0] = 0x6; // write Configuration Port 0  
i2c_drv_Buffer[1] = 0;   // Configuration Port 0
i2c_drv_Buffer[2] = 0;   // Configuration Port 1
i2c_drv_Write(I2C0, 0x21, i2c_drv_Buffer, 3);


// ##### Setup PCA8535 - Discrete Inputs
i2c_drv_Buffer[0] = 0x6;   // write Configuration Port 0  
i2c_drv_Buffer[1] = 0xFF;  // Configuration Port 0
i2c_drv_Buffer[2] = 0x3;   // Configuration Port 1
i2c_drv_Write(I2C0, 0x20, i2c_drv_Buffer, 3);


// ##### Setup PCA8535 - Port A, Port B - outputs/inputs
i2c_drv_Buffer[0] = 0x2; // write Output Port 0  
i2c_drv_Buffer[1] = 0;
i2c_drv_Buffer[2] = 0;
i2c_drv_Write(I2C0, 0x23, i2c_drv_Buffer, 3);

i2c_drv_Buffer[0] = 0x6;  // write Configuration Port 0  
i2c_drv_Buffer[1] = 0xF;  // Configuration Port 0
i2c_drv_Buffer[2] = 0xF;  // Configuration Port 1
i2c_drv_Write(I2C0, 0x23, i2c_drv_Buffer, 3);


// ##### Read the Temperature Sensor ID
i2c_drv_Buffer[0] = 0xFF; // read Device ID
i2c_drv_Write(I2C1, 0x4C, i2c_drv_Buffer, 1);
i2c_drv_Read(I2C1, 0x4C, i2c_drv_Buffer, 1);
pfm_TemperatureSensor.ID = i2c_drv_Buffer[0]; 


pfm_Timer = 500;
while(pfm_Timer);

//////////////////////////////////

uint32_t data_len;

if(usbfs_core_dev.dev.status == USB_STATUS_CONFIGURED)
   {
   cdc_acm_data_receive(&usbfs_core_dev);

   data_len = snprintf((char*)&usb_data_buffer, CDC_ACM_DATA_PACKET_SIZE, "\r\n\r\n*** PARKING FIRMWARE VER.%s ***\r\n", PFM_VERSION); 
   cdc_acm_data_send(&usbfs_core_dev, data_len);

   pfm_Timer = PFM_CDC_TX_DELAY;
   while(pfm_Timer);
   }


/////////////////////////////////////////////////////////////////////////////////////////////
while(true)
   {
   if(usbfs_core_dev.dev.status ==  USB_STATUS_CONFIGURED)
      {
      if(packet_sent)
         {
         packet_sent = 0;
         cdc_acm_data_receive(&usbfs_core_dev);
         }
      
      if(packet_receive)
         {
         cdc_acm_data_receive(&usbfs_core_dev);
         
         data_len = receive_length;
         
         if(data_len)
            {
            if(data_len > CDC_ACM_DATA_PACKET_SIZE)
               {
               data_len = CDC_ACM_DATA_PACKET_SIZE;
               }
            
            wakelim_Unstuff_It(usb_data_buffer, data_len);

            usb_data_buffer[1] &= 0x7F;

            // check address field and CRC8
            if(((usb_data_buffer[1] == WAKELIM_SELF_ADDRESS) || (usb_data_buffer[1] == WAKELIM_BROADCAST_ADDRESS)) &&
                (wakelim_CRC8(usb_data_buffer, data_len - 1) == usb_data_buffer[data_len - 1]))
               {
               uint32_t size = 0;
               
               Commands cmd = (Commands)usb_data_buffer[2];
               
               switch(cmd)
                  {
                  
                  // ######### PORTS_IDR #########
                  case Commands_CMD_PORTS_IDR:
                     {
                     usb_data_buffer[size++] = 0xC0;
                     usb_data_buffer[size++] = 0x1;
                     usb_data_buffer[size++] = cmd;
                     usb_data_buffer[size++] = 2;
                     usb_data_buffer[size++] = pfm_EIO_PortA_0_Inputs & 0xF;
                     usb_data_buffer[size++] = pfm_EIO_PortB_1_Inputs & 0xF;
                     
                     pfm_Wake_CDC_Send(size);
                     break;
                     }
                     
                     
                  // ######### PORTS_ODRR #########
                  case Commands_CMD_PORTS_ODRR:
                     {
                     // need to invert here...
                     usb_data_buffer[size++] = 0xC0;
                     usb_data_buffer[size++] = 0x1;
                     usb_data_buffer[size++] = cmd;
                     usb_data_buffer[size++] = 2;
                     usb_data_buffer[size++] = (~(pfm_EIO_PortA_0_Outputs >> 4)) & 0xF;
                     usb_data_buffer[size++] = (~(pfm_EIO_PortB_1_Outputs >> 4)) & 0xF;
                     
                     pfm_Wake_CDC_Send(size);
                     break;
                     }
                     
                     
                  // ######### PORTS_ODRW #########
                  case Commands_CMD_PORTS_ODRW:
                     {
                     pfm_EIO_PortA_0_Outputs = usb_data_buffer[4] << 4;
                     pfm_EIO_PortB_1_Outputs = usb_data_buffer[5] << 4;                     
                     
                     if(pfm_m_iConfig & PFM_CONFIG_REPLY_PORTS)
                        {
                        usb_data_buffer[size++] = 0xC0;
                        usb_data_buffer[size++] = 0x1;
                        usb_data_buffer[size++] = cmd;
                        usb_data_buffer[size++] = 0;
                        
                        pfm_Wake_CDC_Send(size);
                        }
               
                     break;
                     }
                     
                     
                  // ######### PORTS_SET #########
                  case Commands_CMD_PORTS_SET:
                     {
                     // no need to invert here...
                     pfm_EIO_PortA_0_Outputs |= usb_data_buffer[4] << 4;
                     pfm_EIO_PortB_1_Outputs |= usb_data_buffer[5] << 4;
                     
                     if(pfm_m_iConfig & PFM_CONFIG_REPLY_PORTS)
                        {
                        usb_data_buffer[size++] = 0xC0;
                        usb_data_buffer[size++] = 0x1;
                        usb_data_buffer[size++] = cmd;
                        usb_data_buffer[size++] = 0;

                        pfm_Wake_CDC_Send(size);
                        }
               
                     break;
                     }
                     
                     
                  // ######### PORTS_RESET #########
                  case Commands_CMD_PORTS_RESET:
                     {
                     // no need to invert here...
                     pfm_EIO_PortA_0_Outputs &= ~(usb_data_buffer[4] << 4);
                     pfm_EIO_PortB_1_Outputs &= ~(usb_data_buffer[5] << 4);
                     
                     if(pfm_m_iConfig & PFM_CONFIG_REPLY_PORTS)
                        {
                        usb_data_buffer[size++] = 0xC0;
                        usb_data_buffer[size++] = 0x1;
                        usb_data_buffer[size++] = cmd;
                        usb_data_buffer[size++] = 0;

                        pfm_Wake_CDC_Send(size);
                        }
               
                     break;
                     }
                     
                     
                  // ######### PORTS_TOGGLE #########
                  case Commands_CMD_PORTS_TOGGLE:
                     {
                     // no need to invert here...
                     pfm_EIO_PortA_0_Outputs ^= usb_data_buffer[4] << 4;
                     pfm_EIO_PortB_1_Outputs ^= usb_data_buffer[5] << 4;
                     
                     if(pfm_m_iConfig & PFM_CONFIG_REPLY_PORTS)
                        {
                        usb_data_buffer[size++] = 0xC0;
                        usb_data_buffer[size++] = 0x1;
                        usb_data_buffer[size++] = cmd;
                        usb_data_buffer[size++] = 0;

                        pfm_Wake_CDC_Send(size);
                        }
               
                     break;
                     }
                  
                  
                  // ######### RELAYS_IDR #########
                  case Commands_CMD_RELAYS_IDR:
                     {
                     //                       0  1  2  3  4  5  6  7  8  9   // DINx
                     uint8_t ch_decode[10] = {0, 2, 4, 6, 9, 1, 3, 5, 7, 8}; // Pxx

                     uint16_t din_data = ((uint16_t)pfm_EIO_Discrete_Inputs_1 << 8) | pfm_EIO_Discrete_Inputs_0;
                     
                     uint16_t din_data_sorted = 0;

                     for(int32_t i = 9; i >= 0; i--)
                        {
                        din_data_sorted |= din_data & (0x1 << ch_decode[i]) ? 1 : 0;
                        if(i > 0)
                           {
                           din_data_sorted <<= 1;
                           }
                        }
                    
                     usb_data_buffer[size++] = 0xC0;
                     usb_data_buffer[size++] = 0x1;
                     usb_data_buffer[size++] = cmd;
                     usb_data_buffer[size++] = 2;
                     usb_data_buffer[size++] = din_data_sorted >> 8;
                     usb_data_buffer[size++] = din_data_sorted;
                     
                     pfm_Wake_CDC_Send(size);
                     break;
                     }                 
                  
                  
                  // ######### RELAYS_ODRR #########
                  case Commands_CMD_RELAYS_ODRR:
                     {
                     //                       0  1  2  3  4  5  6  7  8  9   // DOUTx
                     uint8_t ch_decode[10] = {0, 2, 4, 6, 9, 1, 3, 5, 7, 8}; // Pxx

                     uint16_t dout_data = ((uint16_t)pfm_EIO_Discrete_Outputs_1 << 8) | pfm_EIO_Discrete_Outputs_1;
                     
                     uint16_t dout_data_sorted = 0;

                     for(int32_t i = 9; i >= 0; i--)
                        {
                        dout_data_sorted |= dout_data & (0x1 << ch_decode[i]) ? 1 : 0;
                        if(i > 0)
                           {
                           dout_data_sorted <<= 1;
                           }
                        }
                    
                     usb_data_buffer[size++] = 0xC0;
                     usb_data_buffer[size++] = 0x1;
                     usb_data_buffer[size++] = cmd;
                     usb_data_buffer[size++] = 2;
                     usb_data_buffer[size++] = dout_data_sorted >> 8;
                     usb_data_buffer[size++] = dout_data_sorted;
                     
                     pfm_Wake_CDC_Send(size);
                     break;
                     }
                     
                  
                  
                  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                  // ######### REPORT EN ERROR BY DEFAULT #########
                  default:
                     {
                     usb_data_buffer[size++] = 0xC0;
                     usb_data_buffer[size++] = 0x1;
                     usb_data_buffer[size++] = Commands_CMD_ERR;
                     usb_data_buffer[size++] = 0;

                     pfm_Wake_CDC_Send(size);
                     break;
                     }
                  
                  } // --- switch cmd
  
               } // --- CRC8
               
            } // --- data sizegth
            
         } // --- packed received
         
      } // --- usbfs_core_dev.dev.status


   // I2C IO expanders
   if(!pfm_Timer_IO)
      {
      pfm_Timer_IO = 10;

      // #### Write to the Power Outputs
      i2c_drv_Buffer[0] = 0x2; // write Output Port 0  
      i2c_drv_Buffer[1] = pfm_EIO_Power_Outputs_0;
      i2c_drv_Buffer[2] = pfm_EIO_Power_Outputs_1;
      i2c_drv_Write(I2C0, 0x22, i2c_drv_Buffer, 3);

      // ##### Write to the Discrete Outputs
      i2c_drv_Buffer[0] = 0x2; // write Output Port 0  
      i2c_drv_Buffer[1] = pfm_EIO_Discrete_Outputs_0;
      i2c_drv_Buffer[2] = pfm_EIO_Discrete_Outputs_1;
      i2c_drv_Write(I2C0, 0x21, i2c_drv_Buffer, 3);

      // ##### Read the Discrete Inputs
      i2c_drv_Buffer[0] = 0; // read Input Port 0  
      i2c_drv_Write(I2C0, 0x20, i2c_drv_Buffer, 1);
      i2c_drv_Read(I2C0, 0x20, i2c_drv_Buffer, 2);
      pfm_EIO_Discrete_Inputs_0 = i2c_drv_Buffer[0];
      pfm_EIO_Discrete_Inputs_1 = i2c_drv_Buffer[1];
     
      // ##### Read the Port A, Port B Inputs
      i2c_drv_Buffer[0] = 0; // read Input Port 0  
      i2c_drv_Write(I2C0, 0x23, i2c_drv_Buffer, 1);
      i2c_drv_Read(I2C0, 0x23, i2c_drv_Buffer, 2);
      pfm_EIO_PortA_0_Inputs = i2c_drv_Buffer[0];
      pfm_EIO_PortB_1_Inputs = i2c_drv_Buffer[1];
    
      // ##### Write to the Port A, Port B Outputs
      i2c_drv_Buffer[0] = 0x2; // write Output Port 0  
      i2c_drv_Buffer[1] = pfm_EIO_PortA_0_Outputs;
      i2c_drv_Buffer[2] = pfm_EIO_PortB_1_Outputs;
      i2c_drv_Write(I2C0, 0x23, i2c_drv_Buffer, 3);
      
      } // --- I2C IO expanders
     
     
   // I2C IO sensors
   if(!pfm_Timer_Sensors)
      {
      // ##### Temperature Sensor
   
      uint8_t tem_status;

      i2c_drv_Buffer[0] = 0x2; // read status byte (flags, busy signal)
      i2c_drv_Write(I2C1, 0x4C, i2c_drv_Buffer, 1);
      i2c_drv_Read(I2C1, 0x4C, i2c_drv_Buffer, 1);
      tem_status = i2c_drv_Buffer[0];

      if(!(tem_status & 0x80))
         {
         // not BUSY
         i2c_drv_Buffer[0] = 0x0; // read local temperature. It returns latest temperature
         i2c_drv_Write(I2C1, 0x4C, i2c_drv_Buffer, 1);
         i2c_drv_Read(I2C1, 0x4C, i2c_drv_Buffer, 1);
         pfm_TemperatureSensor.Temperature = i2c_drv_Buffer[0];

         i2c_drv_Buffer[0] = 0xF; // one-shot command
         i2c_drv_Buffer[1] = 0;
         i2c_drv_Write(I2C1, 0x4C, i2c_drv_Buffer, 2);
         }
     
    
      // ##### Humidity and temperature sensor
      
      i2c_drv_Buffer[0] = 0x24;
      i2c_drv_Buffer[1] = 0x16;
      i2c_drv_Write(I2C1, 0x44, i2c_drv_Buffer, 2);

      pfm_Timer_Sensors = 5; // 4ms per low rep. measure
      while(pfm_Timer_Sensors);

      i2c_drv_Read(I2C1, 0x44, i2c_drv_Buffer, 6);

      int16_t tmp_i16 = (i2c_drv_Buffer[0] << 8) | i2c_drv_Buffer[1];

      pfm_HumiditySensor.Temperature = 175.0f * ((float) tmp_i16 / 65535.0f) - 45.0f;

      uint16_t tmp_u16 = (i2c_drv_Buffer[3] << 8) | i2c_drv_Buffer[4];

      pfm_HumiditySensor.Humidity = 100.0f * ((float)tmp_u16 / 65535.0f);

      pfm_Timer_Sensors = 100; 
      } // --- I2C sensors
      
      
   }
/////////////////////////////////////////////////////////////////////////////////////////////
}
//------------------------- END OF FUNCTION --------------------- 





//---------------------------------------------------------------
// Function    : pfm_Systick
// Description : 
//---------------------------------------------------------------
void pfm_Systick(void)
{
if(pfm_Timer) pfm_Timer--;

if(pfm_Timer_IO) pfm_Timer_IO--;

if(pfm_Timer_Sensors) pfm_Timer_Sensors--;
}
//------------------------- END OF FUNCTION ---------------------



//---------------------------------------------------------------
// Function    : pfm_EXTI
// Description : 
//---------------------------------------------------------------
void pfm_EXTI(void)
{
NVIC_SystemReset();
}
//------------------------- END OF FUNCTION ---------------------





//---------------------------------------------------------------
// Function    : pfm_PowerOn
// Description : 
//---------------------------------------------------------------
static void pfm_PowerOn(void)
{
while(true)
   {
   uint32_t pg_step = 0;
   
   pfm_Timer = 1500;
   while(pfm_Timer);

   gpio_bit_set(PCB_OUT_EN_DC5V_GPIO_Port, PCB_OUT_EN_DC5V);
   
   // Wait for negative PG edge
   pfm_Timer = 50;
   while(pfm_Timer)
      {
      if(!gpio_input_bit_get(PCB_IN_PG_DC5V_GPIO_Port, PCB_IN_PG_DC5V))
         {
         pg_step = 1;
         break;
         }
      }
   
   if(pg_step != 1)
      {
      gpio_bit_reset(PCB_OUT_EN_DC5V_GPIO_Port, PCB_OUT_EN_DC5V);
      continue;
      }


   // Wait for positive PG edge  
   pfm_Timer = 50;
   while(pfm_Timer)
      {
      if(gpio_input_bit_get(PCB_IN_PG_DC5V_GPIO_Port, PCB_IN_PG_DC5V))
         {
         pg_step = 2;
         break;
         }
      }

   if(pg_step != 2)
      {
      gpio_bit_reset(PCB_OUT_EN_DC5V_GPIO_Port, PCB_OUT_EN_DC5V);
      continue;
      }
   
   
   // Check positive PG edge is stable
   pfm_Timer = 500;
   while(pfm_Timer)
      {
      if(!gpio_input_bit_get(PCB_IN_PG_DC5V_GPIO_Port, PCB_IN_PG_DC5V))
         {
         pfm_Timer = 500;
         }
      }  

   break;
   }
}
//------------------------- END OF FUNCTION ---------------------







//---------------------------------------------------------------
// Function    : pfm_Wake_CDC_Send
// Description : 
//---------------------------------------------------------------
static void pfm_Wake_CDC_Send(uint32_t size)
{
uint32_t stuff_size;

usb_data_buffer[size] = wakelim_CRC8(usb_data_buffer, size);

size++;

usb_data_buffer[1] |= 0x80;

stuff_size = wakelim_Stuff_It(usb_data_buffer, size, CDC_ACM_DATA_PACKET_SIZE);

if(stuff_size)
   {                     
   cdc_acm_data_send(&usbfs_core_dev, stuff_size);

   pfm_Timer = PFM_CDC_TX_DELAY;
   while(pfm_Timer);
   }
}                          
//------------------------- END OF FUNCTION ---------------------




//---------------------------------------------------------------
// Function    : pfm_DIO_Decode
// Description : 
//---------------------------------------------------------------
static uint16_t pfm_DIO_Decode(uint8_t io_1, uint8_t io_0)
{
//                       0  1  2  3  4  5  6  7  8  9   // DIOx
uint8_t ch_decode[10] = {0, 2, 4, 6, 9, 1, 3, 5, 7, 8}; // Pxx

uint16_t dio_data = ((uint16_t)io_1 << 8) | io_0;

uint16_t dio_data_sorted = 0;

for(int32_t i = 9; i >= 0; i--)
   {
   dio_data_sorted |= dio_data & (0x1 << ch_decode[i]) ? 1 : 0;
   if(i > 0)
      {
      dio_data_sorted <<= 1;
      }
   }

return(dio_data_sorted);
}
//------------------------- END OF FUNCTION ---------------------


