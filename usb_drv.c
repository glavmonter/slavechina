//===============================================================================
// File             : usb_drv.c
// Date             : 04.10.21
// MCU              : 
// Notes            : 
//===============================================================================
        

/**** Headers */
    
#include "includes.h"


/**** Global structures */

usb_core_handle_struct usbfs_core_dev =
{
.dev = 
   {
   .dev_desc = (uint8_t *)&device_descriptor,
   .config_desc = (uint8_t *)&configuration_descriptor,
   .strings = usbd_strings,
   .class_init = cdc_acm_init,
   .class_deinit = cdc_acm_deinit,
   .class_req_handler = cdc_acm_req_handler,
   .class_data_handler = cdc_acm_data_handler
   },
.udelay = delay_us,
.mdelay = delay_ms
};

                    
/**** Local definitions */ 


/**** Global constants */


/**** Local constants */


/**** Global variables */   

uint8_t timer_prescaler = 0;
uint32_t usbfs_prescaler = 0;


/**** Local variables */



/**** Function prototypes */

void usb_drv_Init(void);

static void usb_drv_ClockConfig(void);
static void usb_drv_InterruptConfig(void);




//---------------------------------------------------------------
// Function    : usb_drv_Init
// Description : 
//---------------------------------------------------------------
void usb_drv_Init(void)
{
// ### USB init

// Configure USB clock
usb_drv_ClockConfig();

// Timer nvic initialization
timer_nvic_init();

// USB device stack configure
usbd_init(&usbfs_core_dev, USB_FS_CORE_ID);

// USB interrupt configure
usb_drv_InterruptConfig();

// is USB device is enumerated successfully
while(usbfs_core_dev.dev.status != USB_STATUS_CONFIGURED) {};

// delay 10 ms
if(NULL != usbfs_core_dev.mdelay)
   {
   usbfs_core_dev.mdelay(10);
   }
}
//------------------------- END OF FUNCTION ---------------------




//---------------------------------------------------------------
// Function    : usb_drv_ClockConfig
// Description : 
//---------------------------------------------------------------
static void usb_drv_ClockConfig(void)
{
SystemCoreClockUpdate();

uint32_t system_clock = SystemCoreClock;

if(system_clock == 48000000)
   {
   usbfs_prescaler = RCU_CKUSB_CKPLL_DIV1;
   timer_prescaler = 3;
   } 
else if (system_clock == 72000000)
   {
   usbfs_prescaler = RCU_CKUSB_CKPLL_DIV1_5;
   timer_prescaler = 5;
   }
else if(system_clock == 120000000)
   {
   usbfs_prescaler = RCU_CKUSB_CKPLL_DIV2_5;
   timer_prescaler = 9;
   }
else
   {
   while(true);
   }

rcu_usb_clock_config(usbfs_prescaler);
rcu_periph_clock_enable(RCU_USBFS);
}
//------------------------- END OF FUNCTION ---------------------



//---------------------------------------------------------------
// Function    : usb_drv_InterruptConfig
// Description : 
//---------------------------------------------------------------
static void usb_drv_InterruptConfig(void)
{
nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
nvic_irq_enable((uint8_t)USBFS_IRQn, 2U, 0U);

// Enable the power module clock
rcu_periph_clock_enable(RCU_PMU);

// USB wakeup EXTI line configuration
exti_interrupt_flag_clear(EXTI_18);
exti_init(EXTI_18, EXTI_INTERRUPT, EXTI_TRIG_RISING);
exti_interrupt_enable(EXTI_18);

nvic_irq_enable((uint8_t)USBFS_WKUP_IRQn, 1U, 0U);
}
//------------------------- END OF FUNCTION ---------------------





