//===============================================================================
// File             : i2c_drv.h
// Date             : 05.10.21
// Notes            :
//===============================================================================


/**** Global definitions */

#define I2C_DRV_BUFFER_SIZE 32


/**** External variables */

extern uint8_t i2c_drv_Buffer[];



/**** External functions */

extern void i2c_drv_Init(void);
extern void i2c_drv_Write(uint32_t i2c_periph, uint8_t address, uint8_t* buffer, uint32_t length);
extern void i2c_drv_Read(uint32_t i2c_periph, uint8_t address, uint8_t* buffer, uint32_t length);




